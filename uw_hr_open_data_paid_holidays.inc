<?php
/**
 * @file
 * Functions for Paid holidays.
 */

/**
 * Page callback.
 *
 * For each current and future year, display <h2> and table of the paid holiday in that year.
 */
function uw_hr_open_data_paid_holidays_page() {
	$data = uw_value_lists_uw_api_query('v2/events/holidays.json');
	$dates = array();
	while ($item = array_shift($data->data)) {
		$timestamp = strtotime($item->date);
		$getdate = getdate($timestamp);
		$dates[$getdate['year']][$timestamp] = $item->name;
	}
	unset($timestamp, $data);

	$this_year = getdate();
	$this_year = $this_year['year'];

	$page = array();

	$page['precontent'] = array('#markup' => '<p>These days are paid holidays for full-time and part-time regular staff and faculty, and may be paid days for other employees; see <a href="/secretariat/policies-procedures-guidelines/policy-38">Policy 38</a> for details. Canadian Union of Public Employees Local 793 members can refer to article 14 of the <a href="/human-resources/sites/ca.human-resources/files/uploads/files/CUPE2010.pdf">collective agreement between the University of Waterloo and CUPE Local 793 (PDF)</a>.</p>');

	$table_template = array(
		'#theme' => 'table',
		'#header' => array('Holiday', 'Date'),
	);
	foreach ($dates as $year => $year_dates) {
		if ($year < $this_year) {
			continue;
		}

		ksort($year_dates);

		$page[$year]['header'] = array('#markup' => '<h2>' . $year . '</h2>');
		$rows = array();
		foreach ($year_dates as $date => $name) {
			$row = array($name, date('l, F j', $date));
			$rows[] = array('data' => $row, 'no_striping' => TRUE);
		}
		$table_template['#rows'] = $rows;
		$page[$year]['table'] = $table_template;
	}

	return $page;
}
